# FOAM-Extend 4.0 for Ubuntu 20.04

Use this repository to compile [FOAM-Extend 4.0](https://sourceforge.net/p/foam-extend/foam-extend-4.0/ci/master/tree/ReleaseNotes.txt) in Ubuntu 20.04.

**Note:** ParaView 4.4 is set to compile without Python.

## Installation

The default installation directory for this repository is `$HOME/Apps/FOAM-Extend/`. If other installation directory is desired, the user must modify the file `etc/bashrc`. To install into the default location run:

```bash
mkdir ~/Apps/FOAM-Extend
cd ~/Apps/FOAM-Extend
git clone https://gitlab.com/serfriz/foam-extend-4.0-ubuntu-20.04.git foam-extend-4.0
```

### Step 1

Add the repository from Ubuntu 16.04 to be able to install gcc-5 and g++-5:

```bash
echo "deb http://archive.ubuntu.com/ubuntu xenial main" | sudo tee /etc/apt/sources.list.d/xenial.list
```

### Step 2

Add [this](https://launchpad.net/~rock-core/+archive/ubuntu/qt4/) PPA repository to install qt4:

```bash
sudo add-apt-repository ppa:rock-core/qt4
sudo apt-get update
```

### Step 3

Run the following command to install the required libraries:

```bash
sudo apt-get install git-core build-essential binutils-dev cmake flex \
zlib1g-dev qt4-dev-tools libqt4-dev libncurses5-dev \
libxt-dev rpm mercurial graphviz python python-dev  gcc-5 g++-5
```

### Step 4

Now, you can set the environment variables and create an alias for it with the following commands:

```bash
cd ~/Apps/FOAM-Extend/foam-extend-4.0
source etc/bashrc
echo "alias fe40='source \$HOME/Apps/FOAM-Extend/foam-extend-4.0/etc/bashrc'" >> $HOME/.bashrc
```

**Note:** This last line means that whenever you start a new terminal window or tab, you should run the *alias* command associated to the `foam-extend 4.0` shell environment. In other words, whenever you start a new terminal, you should run: `fe40`

### Step 5

Then before running `Allwmake.firstInstall`, run the following commands:

```bash
sed -i -e 's=rpmbuild --define=rpmbuild --define "_build_id_links none" --define=' ThirdParty/tools/makeThirdPartyFunctionsForRPM
sed -i -e 's/gcc/\$(WM_CC)/' wmake/rules/linux64Gcc/c
sed -i -e 's/g++/\$(WM_CXX)/' wmake/rules/linux64Gcc/c++
```

### Step 6

Start compiling:

```bash
./Allwmake.firstInstall > log.firstInstall 2>&1
```

## References

https://openfoamwiki.net/index.php/Installation/Linux/foam-extend-4.0

https://openfoamwiki.net/index.php/Installation/Linux/foam-extend-4.0/Ubuntu

https://gitlab.kitware.com/paraview/paraview/-/issues/15655

https://www.cfd-online.com/Forums/openfoam-installation/173073-foam-extend-3-2-ubuntu-16-04-lts.html

https://www.cfd-online.com/Forums/openfoam-installation/91769-how-get-python-shell-into-paraview-3-8-1-a.html

https://askubuntu.com/questions/1235819/ubuntu-20-04-gcc-version-lower-than-gcc-7

https://askubuntu.com/questions/1234786/qt4-libqt4-in-ubuntu-20-04

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
